from PIL import Image
import aiohttp
import discord
from discord.ext import commands
from io import BytesIO
import asyncio

MAX_RESOLUTION = 4000 # Max resolution of an image to be processed
MAX_SIZE = 10000000 # Max size of an image in bytes (10 MB)

class Images(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.name = "Image Manipulation"
        self.user_images = {} # Links to users' active images
        self.user_editing = {}
    
    @commands.command(help="Opens a linked or attached image for editing.", name="open")
    async def open_image(self, ctx, link=None):
        if link is None:
            if len(ctx.message.attachments) == 0:
                await ctx.send("You must either link or attach an image!")
                return
            link = ctx.message.attachments[0].url

        try:

            if not str(ctx.author.id) in self.user_editing.keys():
                self.user_editing[str(ctx.author.id)] = False

            if self.user_editing[str(ctx.author.id)]:
                await ctx.send("You currently have another editing operation pending. Please wait.")
                return
            
            self.user_editing[str(ctx.author.id)] = True

            async with aiohttp.ClientSession() as session:
                async with session.get(link) as resp:
                    size = int(resp.headers["Content-Length"])
                    if size > MAX_SIZE:
                        await ctx.send("Image too large! Maximum size is 10MB.")
                        self.user_editing[str(ctx.author.id)] = False
                        return
                    
                    await ctx.send("Processing...")

                    image_bytes = BytesIO(await resp.read())

                    image_bytes.seek(0)

                    def convert_to_png(original):
                        image = Image.open(original)
                        x, y = image.size
                        if max(x, y) > MAX_SIZE:
                            return None
                        result = BytesIO()
                        image.save(result, format="PNG")
                        return result

                    upload_bytes = await self.bot.loop.run_in_executor(None, convert_to_png, image_bytes)

                    if upload_bytes is None:
                        await ctx.send(f"Image too large. Maximum size is {MAX_SIZE}x{MAX_SIZE}")
                        self.user_editing[str(ctx.author.id)] = False
                        return

                    upload_bytes.seek(0)

                    upload_file = discord.File(upload_bytes, filename="image.png")

                    embed = discord.Embed(
                        title = "Open for editing!", 
                        colour = discord.Colour.gold(),
                        description = f"Use `{ctx.prefix}help Image` to see available image manipulation commands."
                        )
                    embed.set_image(url="attachment://image.png")

                    message = await ctx.send(embed=embed, file=upload_file)

                    self.user_images[str(ctx.author.id)] = message.embeds[0].image.url
                    self.user_editing[str(ctx.author.id)] = False

        except Exception as e:
            self.user_editing[str(ctx.author.id)] = False
            await ctx.send(f"An error occurred: \n```\n{e}\n```")
    
    @commands.command(help="Views your or another user's image open for editing.")
    async def view(self, ctx, user:discord.User=None):
        if user is None:
            user = ctx.author
        uid = str(user.id)
        if uid in self.user_images.keys():
            embed = discord.Embed(
                colour = discord.Colour.gold(),
                description = f"{user.mention}'s opened image:"
            )
            embed.set_image(url=self.user_images[uid])
            await ctx.send(embed=embed)
            return
        embed = discord.Embed(
            colour = discord.Colour.gold(),
            description = f"{user.mention} has no image open for editing."
        )
        await ctx.send(embed=embed)

    @commands.command(help="Resizes an image to the specified dimensions.")
    async def resize(self, ctx, x:int, y:int, interpolate=None):
        if (min(x,y) <= 0) or (max(x,y) > MAX_RESOLUTION):
            await ctx.send(f"Target resolution must be between 1 and {MAX_RESOLUTION}, inclusive.")
            return
        
        try:

            if not str(ctx.author.id) in self.user_editing.keys():
                self.user_editing[str(ctx.author.id)] = False

            if self.user_editing[str(ctx.author.id)]:
                await ctx.send("You currently have another editing operation pending. Please wait.")
                return

            self.user_editing[str(ctx.author.id)] = True

            uid = str(ctx.author.id)

            if uid in self.user_images.keys():

                async with aiohttp.ClientSession() as session:
                    async with session.get(self.user_images[str(ctx.author.id)]) as resp:

                        image_bytes = BytesIO(await resp.read())
                        image_bytes.seek(0)

                        def resize(image, x, y, interpolate):
                            img = Image.open(image)

                            mode = Image.NEAREST if interpolate is None else Image.BICUBIC

                            result = img.resize((x, y), resample=mode)

                            res_bytes = BytesIO()

                            result.save(res_bytes, "PNG")

                            res_bytes.seek(0)

                            return res_bytes

                        upload_bytes = await self.bot.loop.run_in_executor(None, resize, image_bytes, x, y, interpolate)

                        upload_file = discord.File(upload_bytes, filename="image.png")

                        embed = discord.Embed(
                            title = "Resized!", 
                            colour = discord.Colour.gold(),
                            )
                        embed.set_image(url="attachment://image.png")

                        message = await ctx.send(embed=embed, file=upload_file)

                        self.user_images[str(ctx.author.id)] = message.embeds[0].image.url
                        self.user_editing[str(ctx.author.id)] = False

                        return
        
        except Exception as e:
            self.user_editing[str(ctx.author.id)] = False
            print(type(e), e)
        
        embed = discord.Embed(
            colour = discord.Colour.gold(),
            description = f"{ctx.author.mention} has no image open for editing."
        )
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Images(bot))