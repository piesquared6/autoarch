import secret
import asyncio
import discord
from discord.ext import commands
from extensions import *
import config

bot = commands.Bot(command_prefix=config.prefix)

bot.load_extension("extensions.help")
bot.load_extension("extensions.handlers")
bot.load_extension("extensions.utils")
bot.load_extension("extensions.hypixel")
bot.load_extension("extensions.image")
bot.load_extension("extensions.crypto")

bot.run(secret.token)